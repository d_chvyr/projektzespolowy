# **_[ALIKACJA](https://projektzespolowy1.herokuapp.com/)_**
## **_Authors:_** 

### **_[Karol Chanaj](https://projektzespolowy.atlassian.net/jira/software/c/projects/PZ1/issues/?jql=project%20%3D%20%22PZ1%22%20AND%20assignee%20IN%20%28%225e0f45603e586d0dafebb3db%22%29%20ORDER%20BY%20created%20DESC)_** **_(FrontEnd)_**

### **_[Denys Chvyr](https://projektzespolowy.atlassian.net/jira/software/c/projects/PZ1/issues/PZ1-275?jql=project%20%3D%20%22PZ1%22%20AND%20assignee%20IN%20%28%225f807c424d09f7007644b06d%22%29%20ORDER%20BY%20created%20DESC)_** **_(BackEnd)_**

### **_[Krzysztof Krawczyk](https://projektzespolowy.atlassian.net/jira/software/c/projects/PZ1/issues/PZ1-236?jql=project%20%3D%20%22PZ1%22%20AND%20assignee%20IN%20%28%225f8061b26029960076024827%22%29%20ORDER%20BY%20created%20DESC)_** **_(QA)_**

### **_[Krystian Wieczorek](https://projektzespolowy.atlassian.net/jira/software/c/projects/PZ1/issues/PZ1-254?jql=project%20%3D%20%22PZ1%22%20AND%20assignee%20IN%20%28%225f8082bf459d420069c0de95%22%29%20ORDER%20BY%20created%20DESC)_** **_(Security)_**

#  _About the project:_
### This is a simple web-based chat application written in Python hosted on Heroku. Realised with Django framework and documented via Atlassian Jira.

# _RUN:_
*__Linux__*
```bash
git clone https://bitbucket.org/d_chvyr/projektzespolowy.git
docker build .
docker-compose up
```
# __Project view__

### **_Login_**
![Scheme](screenshots/login.png)
## **_Sign in_**
![Scheme](screenshots/signin.png)
## **_Settings_**
![Scheme](screenshots/settings.png)
## **_Chat page_**
![Scheme](screenshots/chat_view.png)
