from django.shortcuts import render, get_object_or_404
import json
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from .forms import ChatForm
from .models import Chat, ChatInfo, Message
from auth_user.models import UserSetting
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


#@cache_page(CACHE_TTL)
@login_required(login_url='/login/')
def index(request):
    if request.method == 'GET':
        print(request)
        form = ChatForm()
        return render(request, 'chat/index.html', {
            'chat': form
        })
    else:
        chat_form = ChatForm(request.POST)
        if chat_form.is_valid():
            chat_title = chat_form.cleaned_data.get('chat_title')
            user = request.user
            chat_ = Chat.objects.create(chatOwner=user.id, chatTitle=chat_title)
            chat_.save()
            chat_info = ChatInfo.objects.create(participantId=user.id, is_admin=True, chat_id=chat_.id)
            chat_info.save()
            return HttpResponseRedirect(f'/chat/{chat_title}/{chat_.id}')
        else:
            request.method = 'GET'
            return index(request)


@login_required(login_url='login/')
def room(request, room_name, chat_id=None):
    d = dict()
    user_setting_info = UserSetting.objects.filter(id=request.user.id).first()
    all_users = UserSetting.objects.exclude(id=request.user.id)
    for user in all_users:
        print(Message().last_message(chat_id=chat_id, user_id=user.id))
    d['room_name_json'] = mark_safe(json.dumps(room_name))
    d['user'] = request.user
    d['user_info'] = user_setting_info
    d['chat_id'] = chat_id
    d['users'] = all_users
    user_list = [x.participantId for x in ChatInfo.objects.filter(chat_id=chat_id).all()]
    return render(request, 'chat/message_list.html', {'info': d})


User = get_user_model()
