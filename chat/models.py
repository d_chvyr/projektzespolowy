from django.db import models
from django.contrib.auth import get_user_model
# Create your models here.

User = get_user_model()


class Message(models.Model):
    author = models.ForeignKey(User, related_name="author_message", on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    chat_id = models.IntegerField(default=0)

    def __str__(self):
        return self.author.username + f"{self.content}"

    def last_10_messages(self, chat_id):
        return Message.objects.filter(chat_id=chat_id).order_by('timestamp').all()

    def last_chat_message(self, chat_id):
        return Message.objects.filter(chat_id=chat_id).order_by('-timestamp').last()

    def last_message(self, chat_id, user_id):
        return Message.objects.filter(chat_id=chat_id, author_id=user_id).last()


class Chat(models.Model):
    id = models.AutoField(primary_key=True)
    chatOwner = models.IntegerField(default=0)
    chatTitle = models.TextField(default="", max_length=100)

    def __str__(self):
        return str(self.id) + " " + self.chatTitle


class ChatInfo(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, null=True)
    participantId = models.IntegerField(default=0)
    is_admin = models.BooleanField(default=False)
