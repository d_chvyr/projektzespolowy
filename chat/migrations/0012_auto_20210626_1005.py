# Generated by Django 3.1.2 on 2021-06-26 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0011_auto_20210626_1004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='chatOwner',
            field=models.IntegerField(default=0),
        ),
    ]
