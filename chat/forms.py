from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class ChatForm(forms.Form):

    """
    <input type="text" class="form-control" placeholder="headline" value="UI/UX Developer at Boston">
    """

    chat_title = forms.CharField(label='chat_title', max_length=100,
                                 widget=forms.TextInput(attrs={'placeholder': 'Enter chat title',
                                                               'class': 'form-control'}))
