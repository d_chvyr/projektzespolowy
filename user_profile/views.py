from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
import json
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from auth_user.models import UserSetting
from auth_user.forms import ImageForm
from chat.models import Chat, ChatInfo, Message
# Create your views here.


@login_required(login_url='login/')
def profile(request):
    return HttpResponseRedirect('/chat/main/0')
    if request.user.id is None:
        return HttpResponseRedirect("/login")
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            print(form.cleaned_data)
    else:
        current_user = request.user
        user_info = UserSetting.objects.filter(user_id=request.user.id).first()
        form = ImageForm()
        lst = [x.chat_id for x in ChatInfo.objects.filter(participantId=request.user.id).all()]
        chat_list = Chat.objects.filter(id__in=lst).all()[::-1]
        d = dict()
        d['chat'] = []
        for chat_ in chat_list:
            d['chat'].append({'chat': chat_, 'last_message': Message().last_chat_message(chat_id=chat_.id)})
        print(d)
        return render(request, "profile/profile.html", {'user': current_user,
                                                        'info': user_info,
                                                        'imageForm': form,
                                                        'chats': d})

