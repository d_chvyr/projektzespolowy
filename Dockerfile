FROM python:3
# zmienne środowiśkowe
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV DEBUG=0
ENV PORT=8000

#ustawiamy workdirt
WORKDIR /code

COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY . /code/
RUN chmod 777 ./
RUN python manage.py makemigrations
RUN python manage.py migrate
