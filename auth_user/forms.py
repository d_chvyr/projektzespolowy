from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import UserSetting


class LoginForm(forms.Form):
    '''<input type="password" id="inputPassword" class="form-control" placeholder="Password" required>'''
    username = forms.CharField(label='username', max_length=100,
                               widget=forms.TextInput(attrs={'placeholder': 'User name', 'class': 'form-control'}))

    password = forms.CharField(label='password', max_length=100,
                               widget=forms.TextInput(attrs={'type': 'password',
                                                             'placeholder': 'Password',
                                                             'autocomplete': 'on',
                                                             'class': 'form-control'}))


class ImageForm(forms.Form):

    """ <input type="file" id="imgupload" style="visibility: hidden">"""

    name = forms.CharField()
    file = forms.FileField()
    '''
    file = forms.FileField
    file.widget.attrs.update({'class': 'rounded-circle_',
                              'title': 'Upload new photo',
                              'onclick': "$('#imgupload').trigger('click'); return false;",
                              })
    '''

class UserForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        # If one field gets autocompleted but not the other, our 'neither
        # password or both password' validation will be triggered.
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        widgets = {'username': forms.TextInput(attrs={'type': 'text',
                                                      'id': "inputName",
                                                      'placeholder': 'Name',
                                                      'class': 'form-control'}),
                   'email': forms.EmailInput(attrs={'type': 'email',
                                                    'placeholder': 'Email',
                                                    'class': 'form-control'}),
                   'password': forms.PasswordInput(attrs={'type': 'password', 'placeholder': 'Password',
                                                          'class': 'form-control'})
                   }
