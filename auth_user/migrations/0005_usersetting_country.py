# Generated by Django 3.1.2 on 2021-06-23 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_user', '0004_auto_20210623_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='usersetting',
            name='country',
            field=models.TextField(null=True),
        ),
    ]
