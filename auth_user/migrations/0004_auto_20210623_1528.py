# Generated by Django 3.1.2 on 2021-06-23 15:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth_user', '0003_auto_20210623_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersetting',
            name='uid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='uid', to=settings.AUTH_USER_MODEL),
        ),
    ]
