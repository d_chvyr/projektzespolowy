from django.urls import path, re_path
from django.conf.urls import url
from . import views
from django.views.generic import RedirectView
from django.conf.urls.static import static
from django.conf import settings
from user_profile import views as profile_view
from django.contrib.auth import logout
import os

urlpatterns = [
	path('login/', views.login, name='login'),
	path('', RedirectView.as_view(url='login/')),
	path('sign-up/', views.sign_up, name='sign-up'),
	path('profile/', profile_view.profile, name='profile'),
	path('logout/', views.logout_view, name='logout')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)