from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import (authenticate, login as auth_login)
from django.contrib.auth import logout
from .forms import UserForm, LoginForm, ImageForm
from .models import UserSetting
from django.contrib import messages
from django.contrib.auth.models import User

def sign_up(request):
    if request.method == 'POST':
        profile_form = UserForm(request.POST)
        if profile_form.is_valid():
            if User.objects.filter(email=profile_form.cleaned_data['email'].lower()).first() is not None:
                messages.error(request, f'Email {profile_form.cleaned_data["email"].lower()} is used by another user')
                print(f'Email {profile_form.cleaned_data["email"].lower()} is used by another user')
            else:
                user = profile_form.save(commit=False)
                user.set_password(profile_form.cleaned_data['password'])
                user.save()
                user_setting = UserSetting.objects.create(uid=user, user_id=user.id,
                                                              nickname=profile_form.cleaned_data['username'])
                user_setting.save()
                return login(request)
    user_form = UserForm()
    return render(request, 'signup/sign-up.html', {'user_form': user_form})


def login(request):
    # if request.user.id is not None:
    #     return HttpResponseRedirect('/profile')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('/chat/main/0')
            else:
                messages.error(request, 'Incorrect login or password')
    login_form = LoginForm()
    return render(request, 'login/sign-in.html', {'form': login_form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')
