from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSetting(models.Model):
    uid = models.ForeignKey(User, related_name="uid", on_delete=models.CASCADE)
    user_id = models.IntegerField(unique=True)
    nickname = models.CharField(max_length=120, unique=True)
    description = models.TextField(null=True)
    photo = models.ImageField(null=True, upload_to='media', default="/index.png")
    country = models.TextField(null=True)
    city = models.TextField(null=True)

