 function scroll() {
        let el = document.getElementsByClassName("message");
        if (el.length !== 0) {
            let last = el[el.length - 1];
            last.scrollIntoView();
            console.debug(last);

        }
    }
    function createMessage(data, to_last=true){
        var userName = "{{ info.user.username }}"
        var author = data['author'];
        var message = document.createElement('div');
        var imgTag  = document.createElement('img');
        var text_main = document.createElement('div');
        var text_group = document.createElement('div');
        var text = document.createElement('div');
        var p = document.createElement('p');
        p.textContent = data.content;
        text_main.className = "text-main";
        var time = document.createElement("span");
        var date = new Date(data['timestamp']);
        time.textContent =
            (date.getHours() < 10 ? "0" + date.getHours(): date.getHours()) +
            ":" +
            (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
        if (author === userName) {
            message.className = 'message me';
            text_group.className = "text-group me";
            text.className = "text me";
        } else {
            message.className = 'message';
            text_group.className = "text-group";
            text.className = "text";
        }
        imgTag.className = "avatar-md";
        imgTag.src = data['author_photo'];
        text.appendChild(p);
        text_group.appendChild(text);
        text_main.appendChild(text_group);
        text_main.appendChild(time);
        if (author !== userName) {
            message.appendChild(imgTag);
        }
        message.appendChild(text_main)
        if (to_last)
            console.debug(message);
        document.querySelector('#message-log').appendChild(message)
        if(to_last)
            scroll();
        //el.scrollIntoView();
        //document.querySelector('#message-log').scroll = 0;
    }